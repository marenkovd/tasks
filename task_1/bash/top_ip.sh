#!/bin/bash

function usage {
    echo "Usage: $0 [OPTIONS] FILENAME "
	echo "List top occurring ipv4 addresses in the FILENAME"
	echo "OPTIONS:"
	echo "-n NUM		    list only NUM most occurring addresses ;  default is 10"
	echo "-r		    list the least occurring addrs"
	echo "-p		    prefix addrs by the number of occurrence"
	echo "-h		    display this help"
}

#Defaults.
OCC_NUM=10
SORTING="-r"
PRINT_NUM=false

while getopts "rphn::" opt; do
	case $opt in
		n)
			OCC_NUM=$OPTARG
			re='^[0-9]+$'
			if ! [[ $OCC_NUM =~ $re ]] ; then
	   			echo "Error: NUM must be integer" >&2;
				usage
				exit 1
			fi
			;;
		r)
			SORTING=""
			;;
		p)
			PRINT_NUM=true
			;;
		h)
			usage
			exit 0
			;;
		 :)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
		esac
done

shift $(($OPTIND - 1))

if [ $# -lt 1 ]
then
	echo "Missing argument: FILENAME"
	usage
	exit 1
fi

FILE=$1

if [ ! -f $FILE ]; then
    echo "File "$FILE" not found!"
    exit 1
fi

#for ip v4 addrs, including broadcast/multicast etc
REGEXP='(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'

MOST_IPS=$(grep -E -o $REGEXP $FILE | sort | uniq -c | sort $SORTING -n | head -n $OCC_NUM)

if $PRINT_NUM;
then
	cat <<< "$MOST_IPS"
else
	awk -F " " '{print $2}' <<< "$MOST_IPS"
fi

exit 0