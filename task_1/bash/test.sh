#!/bin/sh

#Check small file with number of occurence
./top_ip.sh -p test_1.dat | diff -Ewbs test_1.result.ok -

#Check medium file with number of occurence
./top_ip.sh -p test_2.dat | diff -Ewbs test_2.result.ok -

#Check medium file without number of occurence
./top_ip.sh test_2.dat | diff -Ewbs test_2.result.final -

#Check medium file with reverse result
./top_ip.sh -r test_2.dat | diff -Ewbs test_2.result.final.reverse -

#Check medium file with number of addrs = 7
./top_ip.sh -n 7 test_2.dat | diff -Ewbs test_2.result.final.N7 -