"""
.. module:: top_ip
   :platform: Unix/python3
   :synopsis: Task 1 python implementation

.. moduleauthor:: Marenkovd Dmitry <marenkovd@yandex.ru>

"""

import argparse
import re
import collections


def top_ip(data):

    regexp_ipv4 = '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.' \
              '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'

    recomp = re.compile(regexp_ipv4)
    matches = list(".".join(m) for m in recomp.findall(data))
    return collections.Counter(matches).most_common()

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='List top occurring ipv4 addresses in the FILENAME')
    parser.add_argument('filename', metavar='FILENAME', type=str,
                        help='file with addresses')
    parser.add_argument('-n', '--num', action='store', type=int,
                        metavar='NUM', default=10,
                        help='list only NUM most occurring addresses ;  default is 10')
    parser.add_argument('-r', '--reverse', action='store_true', default=False,
                        help='list the least occurring addrs')
    parser.add_argument('-p', '--prefix', action='store_true', default=False,
                        help='prefix addrs by the number of occurrence')
    args = parser.parse_args()

    with open(args.filename, 'r') as fd:
        data = fd.read()

    ips = top_ip(data)

    if args.reverse:
        result = ips[-args.num:]
    else:
        result = ips[:args.num]

    if args.prefix:
        for ip in result:
            print('{} {}'.format(ip[1], ip[0]))
    else:
        for ip in result:
            print('{}'.format(ip[0]))
