"""
.. module:: tests
   :platform: Unix/python3
   :synopsis: Task 1 unit tests

.. moduleauthor:: Marenkovd Dmitry <marenkovd@yandex.ru>

"""

import unittest

import top_ip


class MyTestCase(unittest.TestCase):
    def test_one(self):
        filedata = 'test_1.dat'

        expected = [
            ('10.0.0.153', 16),
            ('64.242.88.10', 5),
            ('203.147.138.233', 4),
            ('207.195.59.160', 4),

        ]

        with open(filedata) as fd:
            data = fd.read()
        result = top_ip.top_ip(data)

        self.assertEqual(set(result), set(expected), 'Wrong output')

        expected_ind = list(i[1] for i in expected)
        resulted_ind = list(i[1] for i in result)
        self.assertEqual(expected_ind, resulted_ind, 'Wrong order')

    def test_two(self):
        filedata = 'test_2.dat'
        expected = [
            ('10.0.0.153', 27),
            ('208.247.148.12', 8),
            ('142.27.64.35', 7),
            ('145.253.208.9', 5),
            ('64.242.88.10', 4),
            ('61.165.64.6', 4),
            ('212.92.37.62', 4),
            ('61.9.4.61', 3),
            ('207.195.59.160', 3),
            ('203.147.138.233', 2),
            ('128.227.88.79', 2),
            ('195.246.13.119', 2),
            ('216.139.185.45', 1),
            ('219.95.17.51', 1),
            ('212.21.228.26', 1),
            ('66.213.206.2', 1),
            ('195.230.181.122', 1),
            ('64.246.94.152', 1),
        ]

        with open(filedata) as fd:
            data = fd.read()
        result = top_ip.top_ip(data)

        self.assertEqual(set(result), set(expected), 'Wrong output')

        expected_ind = list(i[1] for i in expected)
        resulted_ind = list(i[1] for i in result)
        self.assertEqual(expected_ind, resulted_ind, 'Wrong order')

    def test_generic(self):
        filedata = 'test_2.dat'
        with open(filedata) as fd:
            data = fd.read()
        result = top_ip.top_ip(data)

        resulted_ips = list(i[0] for i in result)
        self.assertEqual(len(resulted_ips), len(set(resulted_ips)), 'Duplicate ip addrs')

        resulted_ind = list(i[1] for i in result)
        self.assertEqual(resulted_ind, sorted(resulted_ind, reverse=True), 'Wrong order')

if __name__ == '__main__':
    unittest.main()
