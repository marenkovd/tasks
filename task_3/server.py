"""
.. module:: server
   :platform: Unix/python3
   :synopsis: Task 3 mock-api

.. moduleauthor:: Marenkovd Dmitry <marenkovd@yandex.ru>

"""

import flask

app = flask.Flask('Task_3 server')

Clients = [
    dict(ID=2, Name='Иванов И. П'),
    dict(ID=14, Name='Петров П. П'),
    dict(ID=32, Name='Сидоров С. С')
]

Products = [
    dict(ID=19, Description='Test item 1'),
    dict(ID=21, Description='Test item 2'),
    dict(ID=34, Description='Test item 3')
]

Sales = [
    dict(ID=1, ProductID=21, Price=5500, Number=1, Date='2014-01-26', ClientID=14),
    dict(ID=2, ProductID=19, Price=900, Number=2, Date='2014-01-27', ClientID=2),
    dict(ID=3, ProductID=21, Price=7000, Number=1, Date='2014-01-27', ClientID=32),
    dict(ID=4, ProductID=34, Price=2200, Number=4, Date='2014-01-27', ClientID=14),
]


@app.route('/api/', methods=['POST'])
def api():
    reltype = flask.request.args.get('type')

    if reltype == 'clients':
        result = flask.jsonify(result=Clients)
    elif reltype == 'products':
        result = flask.jsonify(result=Products)
    elif reltype == 'sales':
        result = flask.jsonify(result=Sales)
    else:
        result = 'Invalid type', 422
    return result

if __name__ == '__main__':
    app.run()