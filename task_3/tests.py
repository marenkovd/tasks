"""
.. module:: tests
   :platform: Unix/python3
   :synopsis: Task 3 tests

.. moduleauthor:: Marenkovd Dmitry <marenkovd@yandex.ru>

"""

import unittest
import multiprocessing
import time

import server
import client


class MyTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.url = 'http://localhost:5000/api'
        cls._server = multiprocessing.Process(target=server.app.run)
        cls._server.start()
        time.sleep(2)

    @classmethod
    def tearDownClass(cls):
        cls._server.terminate()
        cls._server.join()

    def test_clients(self):
        expected = server.Clients
        result = client.get_json_data(self.url, 'clients')['result']
        self.assertEqual(result, expected)

    def test_products(self):
        expected = server.Products
        result = client.get_json_data(self.url, 'products')['result']
        self.assertEqual(result, expected)

    def test_sales(self):
        expected = server.Sales
        result = client.get_json_data(self.url, 'sales')['result']
        self.assertEqual(result, expected)

    def test_invalid(self):
        with self.assertRaises(Exception):
           client.get_json_data(self.url, 'other_type_invalid_one')


if __name__ == '__main__':
    unittest.main()