"""
.. module:: client
   :platform: Unix/python3
   :synopsis: Task 3 impl

.. moduleauthor:: Marenkovd Dmitry <marenkovd@yandex.ru>

"""

import requests

def get_json_data(url, reltype):
    r = requests.post(url.strip('/') + '/?type={}'.format(reltype))

    if r.status_code != 200:
        raise requests.exceptions.RequestException('status code is {}'.format(r.status_code))

    if r.headers['content-type'] != 'application/json':
        raise requests.exceptions.RequestException('content-type is {}'.format(r.headers['content-type']))

    return r.json()

def test():
    print(get_json_data('http://localhost:5000/api', 'clients'))
    print(get_json_data('http://localhost:5000/api/', 'products'))
    print(get_json_data('http://localhost:5000/api', 'sales'))

def test_raw():
    print(get_json_data('http://someurl.com/api', 'clients'))
    print(get_json_data('http://sub1.someurl.com/api', 'products'))
    print(get_json_data('http://sub2.someurl.com/api', 'sales'))

if __name__ == '__main__':
    test()
