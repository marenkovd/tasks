        SELECT Name
        FROM Clients
        WHERE ID IN (
          SELECT ClientID
          FROM Sales
          WHERE Date = '2014-01-27'
          GROUP BY ClientID
          HAVING SUM(Price) > 4000
        )
        ORDER BY Name ASC;