"""
.. module:: tests
   :platform: Unix/python3
   :synopsis: Task 2 tests

.. moduleauthor:: Marenkovd Dmitry <marenkovd@yandex.ru>

"""

import sqlite3
import unittest


POPULATE = '''
        CREATE TABLE Products(
          ID INTEGER PRIMARY KEY,
          Description TEXT
        );

        CREATE TABLE Clients(
          ID INTEGER PRIMARY KEY,
          Name TEXT
        );

        CREATE TABLE Sales(
          ID INTEGER PRIMARY KEY,
          ProductID INTEGER,
          Price INTEGER,
          Number INTEGER,
          Date  TEXT,
          ClientID INTEGER,
          FOREIGN KEY(ClientID) REFERENCES Clients(ID),
          FOREIGN KEY(ProductID) REFERENCES Products(ID)
        );

        INSERT INTO Clients VALUES
        (2, 'Иванов И. П'), (14, 'Петров П. П'), (32, 'Сидоров С. С');

        INSERT INTO Products VALUES
        (19, 'Test item 1'), (21, 'Test item 2'), (34, 'Test item 3');

        INSERT INTO Sales VALUES
        (1, 21, 5500, 1, '2014-01-26', 14), (2, 19, 900, 2, '2014-01-27', 2),
        (3, 21, 7000, 1, '2014-01-27', 32), (4, 34, 2200, 4, '2014-01-27', 14),
        (5, 19, 700, 1, '2014-01-28', 32), (6, 34, 3200, 1, '2014-01-28', 14),
        (7, 21, 2700, 3, '2014-01-28', 32), (8, 19, 2200, 2, '2014-01-28', 14),
        (9, 21, 500, 3, '2014-01-28', 2), (10, 34, 300, 2, '2014-01-28', 2),
        (11, 19, 2000, 3, '2014-01-28', 2), (12, 19, 1000, 2, '2014-01-28', 2)
'''

SQL_SCRIPT = '''

        SELECT Name
        FROM Clients
        WHERE ID IN (
          SELECT ClientID
          FROM Sales
          WHERE Date = {}
          GROUP BY ClientID
          HAVING SUM(Price) > {}
        )
        ORDER BY Name ASC
'''


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.conn = sqlite3.connect(':memory:')
        self.conn.executescript(POPULATE)

    def test_low(self):
        expected = [

            ('Иванов И. П',),
            ('Петров П. П',),
            ('Сидоров С. С',)

        ]
        result = self.conn.execute(SQL_SCRIPT.format('\'2014-01-28\'', 3000)).fetchall()
        self.assertEqual(result, expected)

    def test_mid(self):
        expected = [

            ('Иванов И. П',),
            ('Петров П. П',),

        ]

        result = self.conn.execute(SQL_SCRIPT.format('\'2014-01-28\'', 3500)).fetchall()
        self.assertEqual(result, expected)

    def test_high(self):
        expected = [

            ('Петров П. П',),

        ]

        result = self.conn.execute(SQL_SCRIPT.format('\'2014-01-28\'', 4000)).fetchall()
        self.assertEqual(result, expected)

    def test_final(self):
        expected = [

            ('Сидоров С. С',)

        ]
        result = self.conn.execute(SQL_SCRIPT.format('\'2014-01-27\'', 4000)).fetchall()
        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()